<?php
    // Template Name: Todas Noticias
    get_header();

    $args = array (
        'posts_per_page' => 10,
        'post_type' => 'noticias',
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC'
    );
    $the_query = new WP_Query( $args );
?>
<main id="all-noticias" class="container">
        <h1>Todas as Notícias</h1>
        <!-- <p>Na há notícias disponíveis!</p> -->
        <div class="news">
            <?php if ( $the_query->have_posts() ) : 
                while ( $the_query->have_posts() ) : 
                    $the_query->the_post(); ?>
                <div class="card">
                    <h5><?php the_title()?></h5>
                    <p><?php echo wp_strip_all_tags( get_the_content() ) ?></p>
                    <a href="<?php the_permalink()?>">Continuar lendo</a>
                </div>
                <?php endwhile;
            else:?>
                <p><?php esc_html_e( 'Não há notícias no momento!' ); ?></p>
            <?php endif; wp_reset_postdata(); ?>
        </div>
        <div class="paginate-links">
        </div>
    </main>
<?php
    get_footer()
?>