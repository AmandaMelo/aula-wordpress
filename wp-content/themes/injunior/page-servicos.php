<?php
    // Template Name: Servicos
    get_header()
?>
<main id="servicos">
        <h1>Serviços</h1>
        <section class="container">
            <div class="card">
                <div><h2><?php the_field('titulo_servico1')?></h2></div>
                <ul>
                    <p><?php the_field('servico1')?></p>
                </ul>
            </div>
            <div class="card">
                <div><h2><?php the_field('titulo_servico2')?></h2></div>
                <ul>
                    <p><?php the_field('servico2')?></p>
                </ul>
            </div>
            <div class="card">
                <div><h2><?php the_field('titulo_servico3')?></h2></div>
                <ul>
                    <p><?php the_field('servico3')?></p>
                </ul>
            </div>
        </section>
</main>
<?php
    get_footer()
?>