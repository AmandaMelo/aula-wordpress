<?php
    // Template Name: Noticias
    get_header();
    $args = array (
        'posts_per_page' => 4,
        'post_type' => 'noticias',
        'post_status' => 'publish',
        'supress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC'
    );

    // The Query
    $the_query = new WP_Query( $args );
    
    /* Restore original Post Data */
    wp_reset_postdata();
?>
 <main id="noticias">
        <section id="sct-2">
            <div class="container">
                <h1>Últimas Notícias</h1>
                <div class="last-news">
                    <?php if ( $the_query->have_posts() ) : 
                    while ( $the_query->have_posts() ) : 
                        $the_query->the_post(); ?>
                    <div class="card">
                        <h5><?php the_title()?></h5>
                        <p><?php echo wp_strip_all_tags( get_the_content() ) ?></p>
                        <a href="<?php the_permalink()?>">Continuar lendo</a>
                    </div>
                    <?php endwhile;
                else:?>
                    <p><?php esc_html_e( 'Não há notícias no momento!' ); ?></p>
                <?php endif; ?>
                </div>
                <a href="/todas-noticias">Ver Todas</a>
                <p style="display: none;">Não há notícias no momento!</p>
                <div class="card links-uteis">
                    <h4>Links Úteis</h4>
                    <ul>
                        <li><a href="https://g1.globo.com/economia/">G1 | Economia</a></li>
                        <li><a href="https://epocanegocios.globo.com/Inovadores/">Época | Inovações</a></li>
                        <li><a href="https://forbes.com.br/negocios/">Forbes | Negócios</a></li>
                        <li><a href="https://www.bbc.com/portuguese/topics/cvjp2jr0k9rt">BBC | Economia</a></li>
                    </ul>
                </div>
            </div>
        </section>
</main>
<?php
    get_footer()
?>